FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update; 
RUN apt install -y tzdate; apt upgrade; apt -y install golang
RUN mkdir /app

COPY bin/* /app

CMD /app/server --port 8080 --host 0.0.0.0