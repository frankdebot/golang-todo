package dao

import (
	"database/sql"
	"fmt"
	"golang-todo/models"
	"log"
	"os"

	"github.com/davecgh/go-spew/spew"
	_ "github.com/lib/pq"
)

const (
	host   = "127.0.0.1"
	port   = 5432
	user   = "fdebot1"
	dbname = "fdebot"
)

type DaoEr interface {
	Connect()
	Test()
	Insert()
}

type Dao struct {
	psqlconn string
	db       *sql.DB
	myerror  error
}

func New() Dao {
	dao := Dao{}

	return dao
}

func (x *Dao) Connect() {
	println("Connect()")
	x.psqlconn = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, os.Getenv("PSQL_PASS"), dbname)
	x.db, x.myerror = sql.Open("postgres", x.psqlconn)

}
func (x *Dao) Test() bool {
	//x.myerror = x.db.Ping()
	spew.Dump(x.psqlconn)
	x.myerror = x.db.Ping()
	return x.myerror == nil
}

func TestConnection() (bool, error) {
	fmt.Println("Test connection")
	d := New()
	d.Connect()
	test := d.Test()

	if !test {
		fmt.Println("Got here ", d.myerror)
		return false, d.myerror
	}

	return true, nil
}
func (x *Dao) Insert(task models.Task) {
	x.Connect()

	stmt := "INSERT INTO tasks (subject, content) VALUES ($1, $2)"

	_, err := x.db.Exec(stmt, task.Subject, task.Content)

	if err != nil {
		panic(err)
	}
}

func (x *Dao) GetAll() models.Tasks {
	x.Connect()

	var res models.Tasks

	stmt := "SELECT * FROM tasks"

	rows1, err := x.db.Query(stmt)
	if err != nil {
		log.Fatal(err)
	}
	for rows1.Next() {
		var id int64
		var subject, content, startdate, enddate string
		if e := rows1.Scan(&id, &subject, &content, &startdate, &enddate); e != nil {

		}
		thisTask := &models.Task{ID: id, Subject: subject, Content: content, DatetimeStart: startdate, DatetimeFinish: enddate}

		res = append(res, thisTask)
	}
	return res
}
