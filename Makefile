default: build
	go build -o bin/main ./src/main.go

swagger:
	swagger generate server swagger.yml

clean:
	rm bin/*
	rm -fr src/restapi

build:
	go mod init golang-todo || true
	swagger generate server -f swagger.yml
	go get -u -f ./...
	go get github.com/lib/pq
	go get -u github.com/davecgh/go-spew/spew
	go build -o bin/server ./cmd/franks-todo-like-golang-api-server/main.go


docker: build
	docker build -t golang-todo ./

runlocal:
	go run cmd/franks-todo-like-golang-api-server/main.go --port 8080
	#$(MAKE) -C ./src

run: build docker
	docker run -p 8080:8080 golang-todo:latest

