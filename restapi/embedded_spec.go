// Code generated by go-swagger; DO NOT EDIT.

package restapi

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"encoding/json"
)

var (
	// SwaggerJSON embedded version of the swagger document used at generation time
	SwaggerJSON json.RawMessage
	// FlatSwaggerJSON embedded flattened version of the swagger document used at generation time
	FlatSwaggerJSON json.RawMessage
)

func init() {
	SwaggerJSON = json.RawMessage([]byte(`{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "http"
  ],
  "swagger": "2.0",
  "info": {
    "title": "Frank's todo-like golang api",
    "version": "0.1.0"
  },
  "paths": {
    "/fail": {
      "get": {
        "tags": [
          "todo"
        ],
        "operationId": "fail_get",
        "responses": {
          "500": {
            "description": "Something went wrong",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/health": {
      "get": {
        "tags": [
          "todo"
        ],
        "operationId": "health_get",
        "responses": {
          "200": {
            "description": "Health state",
            "schema": {
              "$ref": "#/definitions/status"
            }
          }
        }
      }
    },
    "/task": {
      "post": {
        "tags": [
          "todo"
        ],
        "operationId": "task_post",
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/task"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Tasks inserted",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/tasks": {
      "get": {
        "tags": [
          "todo"
        ],
        "operationId": "tasks_get",
        "responses": {
          "200": {
            "description": "List of all tasks",
            "schema": {
              "$ref": "#/definitions/tasks"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "error": {
      "type": "object",
      "required": [
        "message"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "format": "int64"
        },
        "message": {
          "type": "string"
        }
      }
    },
    "status": {
      "type": "object",
      "required": [
        "status"
      ],
      "properties": {
        "status": {
          "type": "string"
        }
      }
    },
    "task": {
      "type": "object",
      "properties": {
        "content": {
          "type": "string"
        },
        "datetime_finish": {
          "type": "string"
        },
        "datetime_start": {
          "type": "string"
        },
        "id": {
          "type": "integer"
        },
        "subject": {
          "type": "string"
        }
      }
    },
    "tasks": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/task"
      }
    }
  }
}`))
	FlatSwaggerJSON = json.RawMessage([]byte(`{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "http"
  ],
  "swagger": "2.0",
  "info": {
    "title": "Frank's todo-like golang api",
    "version": "0.1.0"
  },
  "paths": {
    "/fail": {
      "get": {
        "tags": [
          "todo"
        ],
        "operationId": "fail_get",
        "responses": {
          "500": {
            "description": "Something went wrong",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/health": {
      "get": {
        "tags": [
          "todo"
        ],
        "operationId": "health_get",
        "responses": {
          "200": {
            "description": "Health state",
            "schema": {
              "$ref": "#/definitions/status"
            }
          }
        }
      }
    },
    "/task": {
      "post": {
        "tags": [
          "todo"
        ],
        "operationId": "task_post",
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/task"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Tasks inserted",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/tasks": {
      "get": {
        "tags": [
          "todo"
        ],
        "operationId": "tasks_get",
        "responses": {
          "200": {
            "description": "List of all tasks",
            "schema": {
              "$ref": "#/definitions/tasks"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "error": {
      "type": "object",
      "required": [
        "message"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "format": "int64"
        },
        "message": {
          "type": "string"
        }
      }
    },
    "status": {
      "type": "object",
      "required": [
        "status"
      ],
      "properties": {
        "status": {
          "type": "string"
        }
      }
    },
    "task": {
      "type": "object",
      "properties": {
        "content": {
          "type": "string"
        },
        "datetime_finish": {
          "type": "string"
        },
        "datetime_start": {
          "type": "string"
        },
        "id": {
          "type": "integer"
        },
        "subject": {
          "type": "string"
        }
      }
    },
    "tasks": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/task"
      }
    }
  }
}`))
}
