Creat table for todo application:
```
CREATE TABLE tasks (
    id SERIAL PRIMARY KEY,
    subject TEXT,
    content TEXT,
    datetime_start TIMESTAMP,
    datetime_finish TIMESTAMP
)
```

#Todo-todo's:
- Implement all missings functions (start, stop task, archive task, etc)
- Introduce ORM abstraction of database
- Input/Output validation
- Authentication (We don't want other people to give you tasks ;-) )
