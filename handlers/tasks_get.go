package handlers

import (
	"golang-todo/dao"
	"golang-todo/restapi/operations/todo"

	"github.com/go-openapi/runtime/middleware"
)

type todoTasksImpl struct{}

func NewTodoTasksGetHandler() todo.TasksGetHandler {
	return &todoTasksImpl{}

}

func (impl *todoTasksImpl) Handle(params todo.TasksGetParams) middleware.Responder {
	d := dao.New()
	res := d.GetAll()

	return todo.NewTasksGetOK().WithPayload(res)
}
