package handlers

import (
	"golang-todo/dao"
	"golang-todo/models"
	"golang-todo/restapi/operations/todo"

	"github.com/davecgh/go-spew/spew"
	"github.com/go-openapi/runtime/middleware"
)

type todoTaskImpl struct{}

// func NewTodoTasksGetHandler() todo.TasksGetHandler {
// 	return &todoTasksImpl{}
// }
func NewTodoTaskPostHandler() todo.TaskPostHandler {
	return &todoTaskImpl{}
}

func (impl *todoTaskImpl) Handle(params todo.TaskPostParams) middleware.Responder {
	// Let me test the connection
	//x Dao = dao.tasks
	err, msg := dao.TestConnection()
	ok := "unknown"
	if !err { // False is failing
		ok = "fail: " + msg.Error()
	} else {
		ok = "ok"
	}
	d := dao.New()
	d.Insert(*params.Body)
	spew.Dump(params.Body)
	responseVal := &models.Error{}
	responseVal.Code = 0
	responseVal.Message = &ok
	return todo.NewTaskPostOK().WithPayload(responseVal)
}
