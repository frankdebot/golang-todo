package handlers

import (
	"golang-todo/models"
	"golang-todo/restapi/operations/todo"

	"github.com/go-openapi/runtime/middleware"
)

type todoFailImpl struct{}

func NewTodoFailGetHandler() todo.FailGetHandler {
	return &todoFailImpl{}

}

func (impl *todoFailImpl) Handle(params todo.FailGetParams) middleware.Responder {
	message := string("I'm a teapot, this is confusing :-)")
	responseVal := &models.Error{}
	responseVal.Code = 418
	responseVal.Message = &message
	return todo.NewFailGetInternalServerError().WithPayload(responseVal)
}
