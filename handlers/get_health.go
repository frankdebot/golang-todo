package handlers

import (
	"golang-todo/dao"
	"golang-todo/models"
	"golang-todo/restapi/operations/todo"

	"github.com/go-openapi/runtime/middleware"
)

type todoHealthImpl struct{}

func NewTodoHealthGetHandler() todo.HealthGetHandler {
	return &todoHealthImpl{}

}

func (impl *todoHealthImpl) Handle(params todo.HealthGetParams) middleware.Responder {
	// Let me test the connection
	//x Dao = dao.tasks
	err, msg := dao.TestConnection()
	ok := "unknown"
	if !err { // False is failing
		ok = "fail: " + msg.Error()
	} else {
		ok = "ok"
	}

	responseVal := &models.Status{Status: &ok}
	return todo.NewHealthGetOK().WithPayload(responseVal)
}
